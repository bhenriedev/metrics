# Container Directory

| Name | Type | Image | DNS | Ports |
|---|---|---|---|---|
| Container Dashboard | Dashboard               | b4bz/homer:latest                      | <https://localhost>            | 8080(Only through traefik)       |
| Portainer           | Container Management    | portainer/portainer-ce:latest          | <https://portainer.localhost>  | 9443                             |
| Traefik             | Reverse Proxy           | traefik:latest                         | <https://traefik.localhost>    | 80, 443                          |
| Minio               | S3 Bucket Strogae       | quay.io/minio/minio                    | <https://minio.localhost>      | 9000, 9050                       |
| Grafana             | Metrics Dashboard       | grafana/grafana-oss:latest             | <https://grafana.localhost>    | 3000                             |
| Prometheus          | Metrics Scraper         | prom/prometheus:latest                 | <https://prometheus.localhost> | 9090                             |
| Nginx               | Web Server              |  nginx:alpine                          | <https://test.localhost>       | 86                               |
| Cadvisor            | Docker Metrics Exporter |   gcr.io/cadvisor/cadvisor:v0.45.0     | N/A                            | 8088                             |
| Nginx Exporter      | Nginx Metrics Exporter  | nginx/nginx-prometheus-exporter:0.10.0 | N/A                            | 9113                             |

## Links

Grafana Dashboards
: <https://grafana.com/grafana/dashboards/>

Youtube Demo
: <https://www.youtube.com/watch?v=9TJx7QTrTyo>

Prometheus Docs
: <https://prometheus.io/docs/prometheus/latest/getting_started/>

Portainer Docs
: <https://docs.portainer.io/>

Traefik Docs
: <https://doc.traefik.io/traefik/>

Traefik Labels
: <https://doc.traefik.io/traefik/routing/providers/docker/>

Minio Docs
: <https://min.io/docs/minio/linux/index.html>

<br>
<br>

# Setup

## Create Proxy Docker Network

> We are going to create a "Proxy" Docker Network. This will allow our containers to talk to eachother and use the container name for routing. It will act as a DNS name but only if the containers are on the same network bridge.

```bash
docker network create proxy
```

## (Optional) Update Passwords

### Minio Dashboard

```yaml
  minio:
    environment:
      MINIO_ROOT_USER: admin
      MINIO_ROOT_PASSWORD: admin
```

### Traefik

Traefik can create a middleware to protect any route

```
echo -n "<PASSWORD>" | base64
```

> _**Will have to escape special characters_

Paste the output in your docker-compose.yml in line (traefik.http.middlewares.traefik-auth.basicauth.users=:)

```yaml
service:
  labels:
    - "traefik.http.middlewares.traefik-auth.basicauth.users=username:base64-encoded-password"
    - "traefik.http.routers.<router-name>.middlewares=traefik-auth"
```

You can use this method to make any route secure with username and password

### NGINX enable Metrics

Nginx by default has some metrics available on the stub_status module. Enable it by adding this to the config

```conf
server {
 
  location /metrics {
    stub_status;
  }

}
```

<br>
<br>

# Start Containers

Simply running this command will pull and start all containers

```
docker-compose up -d
```

## Stop All Containers

```bash
docker-compose down 
```

# Create Dashboards

## Add Prometheus as a Datasoruce

1. Go to <https://grafana.localhost>

2. Login credentials are username=admin, password=admin

3. Bottom left corner click on the settings wheel and click on data sources

4. Add a new data source and select prometheus

5. Add the prometheus Endpoint by using the cotnainer name for DNS

![src](./docs/prometheus_data_source.PNG)

6. Click Save and test

## Add Prometheus Endpoints

Add Prometheus Jobs by going into the prometheus/prometheus.yml and adding fields

Prometheus Example

```yaml
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  
  - job_name: 'prometheus'
    # Override the global default and scrape targets from this job every 5 seconds.
    scrape_interval: 5s
    static_configs:
      - targets: ['prometheus:9090']
```

## Create Dashboard

1. On the lefthand menu find the dashboard section and select +import

2. Select Upload JSON File and import the dashboards in the dashboards folder

Additional dashboards Can be found on
<https://grafana.com/grafana/dashboards/>

<br>
<br>

# Gitlab Runner Metrics

To enable gitlab runner metrics you must edit the config.toml of the runner

either bind the volume to this location or
> docker exec -it container-name bash
> cd /etc/gitlab-runner
> vim config.toml

location in container
> /etc/gitlab-runner/config.toml

add the listen_address

```toml
concurrent = 4
check_interval = 5
log_level = "debug"

# Metrics Listen Address
listen_address = "0.0.0.0:9252"

[session_server]
  session_timeout = 1800
  listen_address = "[::]:8093" #  listen on all available interfaces on port 8093

```

<br>
<br>

# Tips

Use the portainer dashboard to easily find container information and get a shell into container or view logs
